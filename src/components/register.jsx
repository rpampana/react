import "./register.css";
function Register() {
  return (
    <div>
      <div className="container h-100">
        <div className="d-flex justify-content-center h-100">
          <div className="user_card">
            <div className="d-flex justify-content-center">
              <h3 id="form-title">REGISTER ACCOUNT</h3>
            </div>
            <div className="d-flex justify-content-center form_container">
              <form method="POST" action="">
                <Inputblocks name="username" type="text" icon="fas fa-user" />

                <Inputblocks name="email" type="email" icon="fas fa-envelope-square" />
                <Inputblocks name="password" type="password" icon="fas fa-key" />
                <Inputblocks name="password2"  type="password"  icon="fas fa-key"  />


                <div className="d-flex justify-content-center mt-3 login_container">
                  <input
                    className="btn login_btn"
                    type="submit"
                    value="Register Account"
                  />
                </div>
              </form>
            </div>

            <div className="mt-4">
              <div className="d-flex justify-content-center links">
                Already have an account?{" "}
                <a href="{% url 'login' %}" className="ml-2">
                  Login
                </a>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
function Inputblocks(props) {
  return (
    <div className="input-group mb-2">
      <div className="input-group-append">
        <span className="input-group-text">
          <i className={props.icon}></i>
        </span>
      </div>
      <input type={props.type} name={props.name} />
    </div>
  );
}
export default Register;
